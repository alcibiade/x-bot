package org.alcibiade.bot.bridge.service;

import jakarta.annotation.PostConstruct;
import org.alcibiade.bot.bridge.domain.Message;
import org.alcibiade.bot.bridge.respository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageService {

    private final Logger logger = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    private MessageRepository messageRepository;

    @PostConstruct
    private void init() {
        Message message = Message.builder().id(1L).messageText("Hello world !").build();
        messageRepository.save(message);
        logger.info("Saved message {}", message);
    }
}
