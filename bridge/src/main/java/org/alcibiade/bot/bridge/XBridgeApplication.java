package org.alcibiade.bot.bridge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XBridgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(XBridgeApplication.class, args);
    }

}
