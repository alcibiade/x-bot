package org.alcibiade.bot.bridge.respository;

import org.alcibiade.bot.bridge.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

}
