package org.alcibiade.bot.bridge;

import org.alcibiade.bot.bridge.respository.MessageRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class XBridgeApplicationTests {

    @Autowired
    private MessageRepository messageRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void aMessageIsCreated() {
        Assertions.assertThat(messageRepository.findAll()).hasSize(1);
    }

}
